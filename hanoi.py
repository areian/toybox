class Peg(object):
    def __init__(self, discs):
        self.discs = discs
        

    def __getitem__(self, y):
        FMTSTR = '{:^' + str(self.discs*2-1) + '}'
        d = self.discs[y]
        if d == 0:
            return FMTSTR.format('|')
        else:
            return FMTSTR.format('-'*(d*2-1))

    def push(disc):
        i = self.discs.count(0) - 1
        if i < 0:
            raise IndexError
        self.discs[i] = disc

    def pop():
        i = self.discs.count(0)
        disc = self.discs[i]
        self.discs[i] = 0
        return disc

class HanoiBoard(object):
    def __init__(self, discs):
        p = []
        p.extend(range(1,discs+1))
        self.peg1 = Peg(p)
        self.peg2 = Peg([0]*discs)
        self.peg3 = Peg([0]*discs)
        self.discs = discs

    def __repr__(self):
        s = self.fmtstr.format('|')*3 + '\n'
        for d in range(self.discs):
            s += self.peg1[d]
            s += self.peg2[d]
            s += self.peg3[d]
            s += '\n'
        return s

    __str__ = __repr__

# s: start peg
# w: waiter peg
# d: destination peg
# i: number of discs to be moved
# b: board to be printed
def solve(s, w, d, i=-1, b=''):
    if i == 0:
        return
    if i == -1:
        i = len(s)
    solve(s, d, w, i-1, b)
    d.push(s.pop())
    if b != '':
        print(b)
    solve(w, s, d, i-1, b)


solve(hb.peg1, hb.peg2, hb.peg3)
solve(hb.peg3, hb.peg2, hb.peg1)
