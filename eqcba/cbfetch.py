#!/usr/bin/env python
# -*- coding: utf-8 -*-

link_base = "http://www.elfquest.com/comic_xml_zoom.php?fd="

def main(args):
  from subprocess import call
  wget = ['wget', '-nH']
  sed = ['sed', '-i', 's/\\"source/\\"\ source/']
  try:
    wget.append('-a' + args[1].strip())
  except IndexError:
    wget.append('-o/dev/null')
  try:
    f = open(args[0])
    books = f.readlines()
    count = 0
    for book in books:
      count = count + 1
      wg = wget + [link_base + book.strip().replace(' ', '%20'), '-Otmp/{0}.xml'.format(count)]
      call(wg)
      call(sed + ['tmp/{0}.xml'.format(count)])
    f.close()
  except IOError:
    print "Couldn't find file: {0}".format(args[0])


if __name__ == "__main__":
  import sys
  main(sys.argv[1:])
