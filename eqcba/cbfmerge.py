#!/usr/bin/env python
# -*- coding: utf-8 -*-

def main(args):
  from xml.dom.minidom import getDOMImplementation, parse
  start = int(args[0])
  end = int(args[1])
  save_file = open("{0}.xml".format(args[2]), 'w')
  
  rdoc = getDOMImplementation().createDocument(None, "album", None)
  rnode = rdoc.createElement("content")
  rdoc.documentElement.appendChild(rnode)
  
  i = start
  while i <= end:
    dom = parse("tmp/{0}.xml".format(i))
    es = dom.getElementsByTagName('image')
    for e in es:
      rnode.appendChild(e.cloneNode(True))
    i = i + 1
  
  save_file.write(rdoc.toprettyxml())


if __name__ == "__main__":
  import sys
  main(sys.argv[1:])