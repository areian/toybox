#!/usr/bin/env python
# -*- coding: utf-8 -*-

def main(args):
  from xml.dom.minidom import parse
  from subprocess import call
  
  wget = ['wget', '-nH']
  try:
    wget.append('-a{0}'.format(args[1]))
  except IndexError:
    wget.append('-o/dev/null')
  
  call(['mkdir', args[0]])
  
  dom = parse("{0}.xml".format(args[0]))
  
  page = 1
  for e in dom.getElementsByTagName('image'):
    call(wget + ['-O{0}/{1}.jpg'.format(args[0], page), e.attributes['source'].value])
    page = page + 1


if __name__ == "__main__":
  import sys
  main(sys.argv[1:])