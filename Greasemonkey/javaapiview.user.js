// ==UserScript==
// @name          Java API view
// @namespace     http://www.areian.net
// @description   Always see the latest version of the Java API documetation
// @include       http://docs.oracle.com/*
// ==/UserScript==

(function() {
	var versionPath = "/javase/8"; //No trailing slash
	var currentPath = window.location.href.split(window.location.hostname)[1];
	if(!(currentPath.indexOf(versionPath) == 0)) {
		var apiDocArr = currentPath.split("/").slice(3);
		var apiDocPath = versionPath;
		
		for(i = 0; i < apiDocArr.length; i++) {
			apiDocPath += "/" + apiDocArr[i];
		}
		window.location.href = "http://" + window.location.hostname + apiDocPath;
	}
})();

